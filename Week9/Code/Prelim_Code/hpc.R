#Author: Julia Lanoue (j.lanoue17@imperial.ac.uk)
#Date: Nov 2017
#Description: Function which uses a timer to run a series of neutral generation simulatons, including a burn in period where species richness is collected at each generation during the burn in period then octaves are collected at specified intervals. The data is then saved to a RData fle 

#Clearing the workspace
rm(list=ls())
graphics.off()

#Functions called within the cluster run
species_richness <- function(pop){
  richness = length(unique(pop))
  return(richness)
}

initialize_min <- function(size){
  pop = rep(1, times = size)
  return(pop)
}

choose_two <- function(value){
  two = sample(value, 2, replace = F)
  return(two)
}

neutral_step_speciation <- function(pop,speciation_rate){
  thing = choose_two(length(pop))
  if(runif(1) > speciation_rate){
    pop[thing[1]] <- pop[thing[2]]
  }
  else{
    pop[thing[1]] <- max(pop)+1
  }
  return(pop)
}

neutral_generation_speciation <- function(x,v){
  for(i in 1:(length(x)/2)){
    x = neutral_step_speciation(x,v)
  }
  return(ceiling(x))
}

species_abundance <- function(pop){
  as.numeric(sort(table(pop), decreasing = T))
}

octaves <- function(pop){
  oct=tabulate(floor(log2(pop)+1))
  return(oct)
}

########################## Cluster Run Function ####################################
cluster_run <- function(speciation_rate, size, wall_time, interval_rich, interval_oct, burn_in_generations, output_file_name){
  
  pop = initialize_min(size)
  i = 0
  burn_rich=c()
  octs = list()
  
  ptm <- proc.time()[3]
  while(proc.time()[3] - ptm <= (wall_time*60)){
    i = i+1
    x = neutral_generation_speciation(pop,speciation_rate)
      if(i <= 200){
        if(i %% interval_rich == 0){
        burn_rich = c(burn_rich,species_richness(x))
        }
      }
      else{
        if(i %% interval_oct == 0){
        octs = c(octs,list(octaves(species_abundance(x))))}
      }
  }
  time = proc.time()[3]-ptm
  end = tail(x, n = 1)
  save(octs, burn_rich,speciation_rate, size, wall_time, interval_rich, interval_oct, burn_in_generations,end,time, file = output_file_name)
}

#####################################################################################

cluster_run(speciation_rate=0.1, size=100, wall_time=10, interval_rich=1, interval_oct=10, burn_in_generations=200, output_file_name="my_test_file_1.rda")
