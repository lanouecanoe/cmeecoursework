#!/bin/bash


# usage: ./Analysis.sh [PLINKIN] [outfile_prefix]



PLINK=$1    # Plink file set indicated by the user
OUT=$2      # outfile prefix indicated by the user


# the following are the necessary file names needed for the various steps

FRQX=$OUT".frqx"
GENO=$OUT".geno"
OVE=$OUT"_Ob_v_Ex_het.pdf"
FPLOT=$OUT"_F.pdf"
HWE=$OUT".hwe"
HWEOUT=$OUT"_HWE_outliers.txt"
DIR=$OUT"_results"

mkdir ../Results/Analysis_Results

# give the commands for running the analyses exactly as you would on the terminal,
# however you should replace the file names with the appropriate variables


plink --bfile $PLINK --freqx --out $OUT   #run plink to calculate genotype proportions


frqx2geno.pl $FRQX $GENO   #convert the plink output to .geno format


Ob_v_Ex.R $GENO $OVE   # plot the observed versus expected heterozygosity


Moving_F.R $GENO $FPLOT      # plot the moving F values


plink --bfile $PLINK --hardy --out $OUT    # run Hardy Weinberg analysis


sort -k9 $HWE | tail -n 50 >$HWEOUT



# Move everything into the results directory

mv $FRQX ../Results/Analysis_Results
mv $GENO ../Results/Analysis_Results
mv $OVE ../Results/Analysis_Results
mv $FPLOT ../Results/Analysis_Results
mv $HWE ../Results/Analysis_Results
mv $HWEOUT ../Results/Analysis_Results
mv $OUT".log" ../Results/Analysis_Results # also save the plink log. This is the record of what you have done


# cleanup 
 
rm $OUT.nosex  #get rid of unnecessary files



