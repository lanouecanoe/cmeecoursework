#!/usr/bin/python

""" A python script which inputs DNA sequences, 
determines and scores the best alignment, and outputs a text file
with the best score, alignment, and sequence  

Author: Julia Lanoue"""

__author__ = 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__date__ = 'Oct 2017'

# These are the two sequences to match
seq2 = "ATCGCCGGATTACGGG"
seq1 = "CAATTCGGAT"
# assign the longest sequence s1, and the shortest to s2
# l1 is the length of the longest, l2 that of the shortest

import csv #importing csv and sys to read the input csv file and sys to use the 'if name == main' function
import sys

def main(argv):  #defining main argument
	align = open('../Data/seq_data.csv','rb') #opening and assigning the input file
	csvread=csv.reader(align)
	for row in csvread: #assigning variable names to the input file
		seq1 = row[0]
		seq2 = row[1]
	align.close()
	bs = bestie(seq1,seq2) #assigning a variable to the output for the bestie function 
	print bs
	score = open('../Results/best_score.txt','w') #creating the output text document
	score.write("Best Align" + '\n')
	score.write(bs[0] + '\n') #writing the best alignment and best score
	score.write(bs[1] + '\n')
	score.write("Best Score" + " " + str(bs[2]) + '\n')
	score.close()

def bestie(seq1,seq2): #bestie function which takes the two sequences, 
	#rearranges them, then calculates the best score and best alignment
	l1 = len(seq1)
	l2 = len(seq2)
	if l1 >= l2:
		s1 = seq1
		s2 = seq2
	else:
		s1 = seq2
		s2 = seq1
		l1, l2 = l2, l1 # swap the two lengths
		
	my_best_align = None
	my_best_score = -1

	for i in range(l1):
		z = calculate_score(s1, s2, l1, l2, i)
		if z > my_best_score:
			my_best_align = "." * i + s2
			my_best_score = z
	return [my_best_align, s1, my_best_score]

# function that computes a score
# by returning the number of matches 
# starting from arbitrary startpoint
def calculate_score(s1, s2, l1, l2, startpoint):
    # startpoint is the point at which we want to start
    matched = "" # contains string for alignement
    score = 0
    for i in range(l2):
        if (i + startpoint) < l1:
            # if its matching the character
            if s1[i + startpoint] == s2[i]:
                matched = matched + "*"
                score = score + 1
            else:
                matched = matched + "-"

    return score

if (__name__ == "__main__"):
    status=main(sys.argv)
