#!/usr/bin/python

"""Excersise where species are organized into a dictonary 
by their Orders
Author: Julia Lanoue
"""

__author__ = 'Julia Lanoue (j.lanoue@imperial.ac.uk)'
__date__ = 'Oct 2017'

taxa = [ ('Myotis lucifugus','Chiroptera'),
         ('Gerbillus henleyi','Rodentia',),
         ('Peromyscus crinitus', 'Rodentia'),
         ('Mus domesticus', 'Rodentia'),
         ('Cleithrionomys rutilus', 'Rodentia'),
         ('Microgale dobsoni', 'Afrosoricida'),
         ('Microgale talazaci', 'Afrosoricida'),
         ('Lyacon pictus', 'Carnivora'),
         ('Arctocephalus gazella', 'Carnivora'),
         ('Canis lupus', 'Carnivora'),
        ]

# Write a short python script to populate a dictionary called taxa_dic 
# derived from  taxa so that it maps order names to sets of taxa. 
# E.g. 'Chiroptera' : set(['Myotis lucifugus']) etc. 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

# Write your script here:

taxa_dic={} #creating an empty dictionary
for species,order in taxa: #looping through the orders and filling them into an empty set
	if order not in taxa_dic:
		taxa_dic[order]=set()
	taxa_dic[order].add(species) #adding species to the dictionary and pairing them with the appropriate Order

print(taxa_dic)

