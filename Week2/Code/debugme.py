#!/usr/bin/python

"""An example on how to use the debugging program pdb """

__author__ = 'Julia Lanoue (j.lanoue@imperial.ac.uk)'
__date__ = 'Oct 2017'

def createabug(x): #bug detected in function at y=y/z  
	y=x**4
	z=0.
	y=y/z
	import pdb; pdb.set_trace() #pdb used to detect bugs
	return y
createabug(25)
