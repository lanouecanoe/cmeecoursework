#!/usr/bin/python

__Author__ = "Julia Lanoue (j.lanoue17@imperial.ac.uk)"
__Date__ = "Nov 2017"

"""Script which provides examples on the timeit function 
"""

#importing packages
import time 
import timeit

def a_not_useful_function():
	"""Function which adds numbers within the range"""
	y=0
	for i in range(100000):
		y=y+i 
	return 0

def a_less_useless_function():
	"""Function which adds numbers within the range"""
	y=0
	for i in xrange(100000):
		y=y+i
	return 0

#using the time function to determine how long function takes to run
start=time.time()
a_not_useful_function()
print "a_less_useful_function takes %f s to run." % (time.time()-start)

#using time function to determine how long the other function takes to run
start=time.time()
a_less_useless_function()
print "a_less_useless_function takes %f s to run." % (time.time() - start)
 
#~ %timeit a_not_useful_function()
#~ %timeit a_less_useless_function()

#for loops vs list comprehensions
my_list=range(1000)

def my_squares_loop(x):
	"""Squares numbers and appends them to list out"""
	out=[]
	for i in x:
		out.append(i**2)
	return out

def my_squares_lc(x):
	"""Squares numbers using a list comprehension and adds to out list"""
	out = [i**2 for i in x]
	return out

#printing lists
print my_squares_loop(my_list)
print my_squares_lc(my_list)

#Comparing times for loop vs list comprehension
#~ %timeit my_squares_loop(my_list)
#~ %timeit my_squares_lc(my_list)

#for loops vs join method
#importing package and converting letters to a list
import string
my_letters=list(string.ascii_lowercase)

def my_join_loop(l):
	""" printing letters after l"""
	out = ''
	for letter in l: 
		out += letter
	return out

def my_join_method(l):
	""" creating a list of letters after l"""
	out = ''.join(l)
	return out

#printing the letters
print my_join_loop(my_letters)
print my_join_method(my_letters)

#comparing the lists
#~ %timeit(my_join_loop(my_letters))
#~ %timeit(my_join_method(my_letters))

def getting_silly_pi():
	"""summing numbers to a variable within a range"""
	y=0
	for i in xrange(100000):
		y=y+i
	return 0
	
def getting_silly_pii():
	"""summing numbers to a variable within a range"""
	y=0
	for i in xrange(100000):
		y+=i
	return 0

#comparing times between the two methods
#~ %timeit(getting_silly_pi())
#~ %timeit(getting_silly_pii())
