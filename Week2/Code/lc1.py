#!/usr/bin/python

"""Excersise creating list comprehensions and 'for' loops 
for data within tuples
Author: Julia Lanoue
"""

__author__ = 'Julia Lanoue (j.lanoue@imperial.ac.uk)'
__date__ = 'Oct 2017'

birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
         )

#(1) Write three separate list comprehensions that create three different
# lists containing the latin names, common names and mean body masses for
# each species in birds, respectively. 
latin_lc=set([entry[0] for entry in birds]) #list comprehension for latin names 
print latin_lc

common_lc=set([entry[1] for entry in birds]) #list comprehenion for common names
print common_lc

body_mass_lc=set([entry[2] for entry in birds]) #list comprehension for body mass
print body_mass_lc

# (2) Now do the same using conventional loops (you can shoose to do this 
# before 1 !). 

latin=set() #empty set for latin names
for entry in birds:  # for loop going through the birds data 
	latin.add(entry[0]) #adding the latin names to the empty set
print latin 

common=set() #empty set for common names
for entry in birds: 
	common.add(entry[1]) #adding the common names to the empty set
print common 

body_mass=set() #empty set for body masses
for entry in birds: 
	body_mass.add(entry[2]) #adding the body masses to the empty set
print body_mass

# ANNOTATE WHAT EVERY BLOCK OR, IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS
