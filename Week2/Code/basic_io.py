#!/usr/bin/python

"""Opens a text file and creates a dictionary """

__author__ = 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__date__ = 'Oct 2017'

f= open ('../Sandbox/test.txt','r') #opens test.txt file and prints each line
for line in f:
	print line,
	
f.close ()

f= open ('../Sandbox/test.txt','r') #prints each line in file if the 
									# is greater than 0 
for line in f:
	if len(line.strip()) > 0:
		print line,
		
f.close()

list_to_save = range(100)

f= open ('../Sandbox/test.txt','w') #creates a new txt file 
for i in list_to_save:
	f.write(str(i) + '\n')
	
f.close()

my_dictionary = {"a key": 10, "another key":11} #builds a dictionary with 2 keys 
import pickle 

f= open('../Sandbox/testp.p','wb') #using pickle to put an object into a file
pickle.dump(my_dictionary,f)
f.close()

f= open('../Sandbox/testp.p','rb') #using pickle to load the file to an object
another_dictionary= pickle.load(f)
f.close()

print another_dictionary 
