My CMEE 2017-18 Coursework Repository

Week 1 - Unix and bash scripts 
Week 2 - Python
Week 3 - R 
Week 4 - Statistics
Week 5 - QGIS
Week 6 - Genomics
Week 7 - Python II
Week 9 - HPC
Project_Proposal - final pdf and .tex file
CMEEMiniProject - Contains code, data, results and write-up used in mini project
