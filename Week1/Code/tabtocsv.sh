#!/bin/bash
#Author: Julia j.lanoue17@imperial.ac.uk
#Script: tabtocsv.sh
#Desc: substitute the tabs into comma delineated
#Arguments: 1-> tab delimited file
#Date: Oct 2017
echo "Creating a comma delimited version of $1 ..."
cat $1 | tr -s "\t" "," >> $1.csv
echo "Done!"
exit
