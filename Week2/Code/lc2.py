#!/usr/bin/python

"""Excersise creating list comprehensions and 'for' loops to categorize 
rainfall data
Author: Julia Lanoue
"""

__author__ = 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__date__ = 'Oct 2017'

# Average UK Rainfall (mm) for 1910 by month
# http://www.metoffice.gov.uk/climate/uk/datasets
rainfall = (('JAN',111.4),
            ('FEB',126.1),
            ('MAR', 49.9),
            ('APR', 95.3),
            ('MAY', 71.8),
            ('JUN', 70.2),
            ('JUL', 97.1),
            ('AUG',140.2),
            ('SEP', 27.0),
            ('OCT', 89.4),
            ('NOV',128.4),
            ('DEC',142.2),
           )

# (1) Use a list comprehension to create a list of month,rainfall tuples where
# the amount of rain was greater than 100 mm.
 
# (2) Use a list comprehension to create a list of just month names where the
# amount of rain was less than 50 mm. 

# (3) Now do (1) and (2) using conventional loops (you can choose to do 
# this before 1 and 2 !). 

#1 list comprehension for years where rainfall is over 100 mm
over_lc= set([entry for entry in rainfall if entry[1]>100])
print over_lc 

#2 list comprehension for years where rainfall is under 100 mm
under_lc=set([entry for entry in rainfall if entry[1]<50])
print under_lc

# 3 
over_100=set() #creating an empty set 
for entry in rainfall: #for loop to identify then add years where rainfall was over 100 mm
	if entry[1]>100:
		over_100.add(entry)
print over_100

under_100=set() #creating an empty set
for entry in rainfall: #for loop to identify then add years where rainfall was under 50 mm
	if entry[1]<50:
		under_100.add(entry)
print under_100

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS


