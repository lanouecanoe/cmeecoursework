#!/usr/bin/python

__author__ = 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__Date__ = 'Nov 2017'

""" Script which uses regular expressions to print a table containing
the kingdom, phylum and species names for four species of blackbird
"""

#importing packages
import re

# Read the file
f = open('../Data/blackbirds.txt', 'r')
text = f.read()
f.close()

# remove \t\n and put a space in:
text = text.replace('\t',' ')
text = text.replace('\n',' ')

# note that there are "strange characters" (these are accents and
# non-ascii symbols) because we don't care for them, first transform
# to ASCII:
text = text.decode('ascii', 'ignore')
text = text.encode('ascii', 'ignore')

# Now write a regular expression my_reg that captures # the Kingdom, 
# Phylum and Species name for each species and prints it out neatly:

Kingdom = re.findall(r'Kingdom\s(\w+)', text)
#~ print Kingdom 
Phylum = re.findall(r'Phylum\s(\w+)', text)
#~ print Phylum 
Species = re.findall(r'Species\s(\w+\s\w+)', text)
#~ print Species

#Combining the tuple values
combined = list(zip(Kingdom, Phylum, Species))

#printing the table headers and printing the tuples in a table format
print "Kingdom       Phylum     Species"
for entry1,entry2,entry3 in combined:
    print "{:<14}{:<11}{}".format(entry1,entry2,entry3)
    
