#Author: Julia Lanoue (j.lanoue17@imperial.ac.uk)
#Date: Oct 2017
#Description: An R script creating a matrix then using the apply function to apply the SOmeOperation function to the matrix

#defining a function containing if statement to multiply the value of v by 100
SomeOperation <- function(v){
  if (sum(v)>0){ 
    return (v*100)
  }
  return(v)
}

#creating a matrix containing 100 random numbers from the normal distribution
M <- matrix(rnorm(100),10,10)

#using the apply function to apply the function to the matrix
print(apply(M,1,SomeOperation))