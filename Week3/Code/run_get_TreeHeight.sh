#!/bin/bash

#Author: Julia Lanoue (j.lanoue17@imperial.ac.uk)
#Date: Oct 2017
#Description: Bash script which tests if the get_TreeHeights.R script works

Rscript get_TreeHeight.R ../Data/trees.csv
