#!/usr/bin/python

__Author__ = 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__Date__ = 'Nov 2017'

""" Script containing examples regular expression search uses
"""

#importing packages
import re 

my_string = "a given string" #setting a string to a variable name
match = re.search(r'\s', my_string) #using re.search function to find matches to a space in the variable

print match
#printing the matches
match.group()
#setting the results of the search to a variable name
match = re.search(r's\w*',my_string)
match.group() #printing the results

match = re.search(r'\d', my_string)
print match

my_string = 'an example'
match = re.search(r'\w*\s', my_string)

#using an if/else statement to find matches and print a statement if one is found
if match:
	print 'found a match:', match.group()
else:
	print 'did not find a match'

match = re.search(r'\d', "it takes 2 to tango")
print match.group()

match = re.search(r'\s\w*\s', 'once upon a time')
match.group()

match = re.search(r'\s\w{1,3}\s', 'once upon a time')
match.group()

match = re.search(r'\s\w*$', 'once upon a time')
match.group()

match = re.search(r'\w*\s\d.*\d', 'take 2 grams of H2O')
match.group()

match = re.search(r'^\w*.*\s', 'once upon a time')
match.group()

match = re.search(r'^\w*.*?\s', 'once upon a time')
match.group()

match = re.search(r'<.+>', 'This is a <EM>first</EM> test')
match.group()

match = re.search(r'<.+?>', 'This is a <EM>first</EM> test')
match.group()

match = re.search(r'\d*\.?\d*','1432+60.22i')
match.group()

match = re.search(r'[AGTC]+','the sequence ATTCGT')
match.group()

#finding and printing the search in one line
re.search(r'\s+[A-Z]{1}\w+\s\w+','The bird-shit frog"s name is Theloderma asper').group()

#seperating the match search into seperate parts
MyStr = 'Samraat Pawar, s.pawar@imperial.ac.uk, Systems biology and ecological theory'
match = re.search(r"([\w\s]*),\s([\w\.@]*),\s([\w\s&]*)", MyStr)
match.group()
match.group(0)
match.group(1)
match.group(2)
match.group(3)
