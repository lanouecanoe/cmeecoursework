#1
r'^abc[ab]+\s\t\d' 
% 'abca \t1'

#find anything beginning with 'a,b,c' and match 'a' and 'b' and repeat that 
#for any words followed by then a space, tab, and numeric character

#2
r'^\d{1,2}\/\d{1,2}\/\d{4}$'
% '11/12/2004'

#find anything beginning with a numeric starting with 1 and only do that for 2 values,
#stop, followed by a dash then find another numeric starting with one and do that for 2 
#more values, stop, another / then find another numeric 4 characters long and ends with a 4

#3
r'\s*[a-zA-Z,\s]+\s*'
% ' aBz '

#find anything starting with a space then the letters a-z, then capital A-Z, followed by
#another space, find that pattern as many times then find another space
