#!/usr/bin/python

"""Function which takes two fasta files and finds the best alignment and 
best alignment score"""

__author__ = 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__date__ = 'Oct 2017'

import sys 

def main(argv): #defining main arguments 
	if len(argv) == 3: 					#opening files when provided files explicitly
		data1 = open(str(argv[1]),'r')
		data2 = open(str(argv[2]),'r')
	else:                               #setting default data files
		data1 = open('../../Week1/Data/fasta/407228412.fasta','r') 
		data2 = open('../../Week1/Data/fasta/407228326.fasta','r')
	
	line = data1.readline()  #reading fasta files
	unwanted = ""
	wanted = ""
	while line:
		line = line.rstrip('\n') #getting rid of first line and newline characters
		if '>' in line:
			unwanted = line
		else:
			wanted = wanted + line
		line = data1.readline()
	
		 
	line2 = data2.readline()  #reading 2nd file 
	unwanted2 = ""
	wanted2 = ""
	while line2:
		line2 = line2.rstrip('\n') #getting rid of first line and newline characters
		if '>' in line2:
			unwanted2 = line2
		else:
			wanted2 = wanted2 + line2
		line2 = data2.readline()
		
	seq1 = wanted   #defining seq1 and seq2
	seq2 = wanted2 
	
	bs = bestie(seq1,seq2) #assigning a variable to the output for the bestie function 
	print bs
	score = open('../Results/best_score_fasta.txt','w') #creating the output text document
	score.write("Best Align" + '\n')
	score.write(bs[0] + '\n') #writing the best alignment and best score
	score.write(bs[1] + '\n')
	score.write("Best Score" + " " + str(bs[2]) + '\n')
	score.close()

def bestie(seq1,seq2): #bestie function which takes the two sequences, 
	#rearranges them, then calculates the best score and best alignment
	l1 = len(seq1)
	l2 = len(seq2)
	if l1 >= l2:
		s1 = seq1
		s2 = seq2
	else:
		s1 = seq2
		s2 = seq1
		l1, l2 = l2, l1 # swap the two lengths
		
	my_best_align = None
	my_best_score = -1

	for i in range(l1):
		z = calculate_score(s1, s2, l1, l2, i)
		if z > my_best_score:
			my_best_align = "." * i + s2
			my_best_score = z
	return [my_best_align, s1,my_best_score]

# function that computes a score
# by returning the number of matches 
# starting from arbitrary startpoint
def calculate_score(s1, s2, l1, l2, startpoint):
    # startpoint is the point at which we want to start
    matched = "" # contains string for alignement
    score = 0
    for i in range(l2):
        if (i + startpoint) < l1:
            # if its matching the character
            if s1[i + startpoint] == s2[i]:
                matched = matched + "*"
                score = score + 1
            else:
                matched = matched + "-"

    return score

if (__name__ == "__main__"):
    status=main(sys.argv)
