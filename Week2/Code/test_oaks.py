#!/usr/bin/python

"""Excersise into using various debugging techniques"""

__author__ = 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__date__ = 'Oct 2017' 

#all the applications used for the script, including debugging apps
import csv  
import sys
import pdb
import doctest

#Define function
def is_an_oak(name):
    """ Returns True if name is starts with 'quercus' #doctests
        >>> is_an_oak('quercuss')
        False
        
        Returns False if name is 'Fagus sylvatica'
        >>> is_an_oak('Fagus sylvatica')
        False
    """
    return name.lower()=='quercus' #removed space after quercus and set
									#function to only return species with quercus
    
print(is_an_oak.__doc__)

def main(argv): 
    f = open('../Data/TestOaksData.csv','rb')  #fixed relative paths
    g = open('../Results/JustOaksData.csv','wb')
    #import pdb; pdb.set_trace()
    taxa = csv.reader(f)
    csvwrite = csv.writer(g)
    oaks = set()
    for row in taxa:
        print row
        print "The genus is", row[0]
        if is_an_oak(row[0]):
            print row[0]
            print 'FOUND AN OAK!'
            print " "
            csvwrite.writerow([row[0], row[1]])    
    return 0
    
if (__name__ == "__main__"):
    status=main(sys.argv)

doctest.testmod()
