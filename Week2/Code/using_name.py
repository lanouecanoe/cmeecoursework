#!/usr/bin/python

"""Script showing how the main function works once it's imported"""

__author__ = 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__date__ = 'Oct 2017'

if __name__ == '__main__':
	print 'This program is being run by itself'
else:
	print 'I am being imported from another module' #only prints one time when it's first imported
