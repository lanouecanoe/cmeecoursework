#!/usr/bin/python

__author__ = 'Julia Lanoue (j.lanoue@imperial.ac.uk)'
__date__ = 'Oct 2017'

"""Script containing series of functions used to illustrate profileme
"""

def a_useless_function(x):
	"""Function which adds numbers from 0 to 1x10^8"""
	y=0
	for i in xrange(100000000):
		y=y+i 
	return 0
	
def a_less_useless_function(x):
	"""Function which adds numbers from 0 to 1x10^5"""
	y=0
	for i in xrange(100000):
		y=y+i
	return 0
	
def some_function(x):
	"""Fuction which prints the value for x and runs the 
	two other useless functions """
	print x
	a_useless_function(x)
	a_less_useless_function(x)
	return 0
	
some_function(1000)
