#!/usr/bin/python

__author__ = 'Julia Lanoue (j.lanoue@imperial.ac.uk)'
__date__ = 'Oct 2017'

"""An example explaining the difference between a local and global 
value"""

_a_global=10 #setting a value outside the function

def a_function(): 
	global _a_global
	_a_global= 5 #setting a new value for the same variable inside the function
	_a_local = 4
	print "Inside the function, the value is ", _a_global
	print "Inside the function, the value is ", _a_local
	return None

a_function()
print "Outside the function, the value ", _a_global #printing new value of _a_global outside the function
