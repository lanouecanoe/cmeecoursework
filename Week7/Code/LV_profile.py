#!/usr/bin/python

__Author__ = 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__Date__ = 'Nov 2017'

"""Script which runs and profiles LV1.py and LV2.py
"""

#importing packages
import cProfile, pstats, StringIO

#Running and profiling LV1
pr = cProfile.Profile() 
pr.enable() 
import LV1 #stating what file to profile
pr.disable() #showing when to start and stop profiling
s = StringIO.StringIO() 
sortby = 'cumulative' #showing cumulative run time
ps = pstats.Stats(pr, stream=s) #calculating stats and creating report on profile
ps.print_stats(0).sort_stats('calls') #printing the first line of report and number of calls
print s.getvalue() 

#Running and profiling LV2
pr = cProfile.Profile()
pr.enable()
import LV2
pr.disable()
s = StringIO.StringIO()
sortby = 'cumulative'
ps = pstats.Stats(pr, stream=s)
ps.print_stats(0).sort_stats('calls')
print s.getvalue()
