#!/usr/bin/python

"""Description - this is an example

An example of a python module"""

__author__ = 'Julia Lanoue'
__date__ = 'Oct 09 2017'

import sys

#define variables here

def main(argv):  #main argument
	print 'This is a boilerplate'
	return 0
	
if (__name__ == "__main__"):
	status = main(sys.argv)
	sys.exit(status)

