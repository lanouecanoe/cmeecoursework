#!/usr/bin/python

"""A python script explaining the sys.argv command"""

__author__ = 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__date__ = 'Oct 2017'

import sys
print "This is the name of the script: ", sys.argv[0] #printing name of script
print "Number of arguments: ", len(sys.argv) #printing number of arguements 
print "The arguements are: ", str(sys.argv) #stating which arguements are included
