#!/bin/bash
#Author Julia j.lanoue17@imperial.ac.uk
#Script csvtospace.sh
#Desc Converts csv files to space seperated files
#Date Oct 2017
echo "Creating space delimited version of $1..."
cat $1 | tr -s "," " " > $1.txt
exit

 
 
