#!/usr/bin/python

__Author__ = 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__Date__ = 'Nov 2017'

"""Script providing example of subprocesses to run a R script
"""

#importing packages
import subprocess

#using subprocess to open TestR.R and creating two files - TestR.Rout for results and TestR_errFile
subprocess.Popen("/usr/lib/R/bin/Rscript --verbose TestR.R > \
../Results/TestR.Rout 2> ../Results/TestR_errFile.Rout",\
shell=True).wait()
