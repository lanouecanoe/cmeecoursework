#!/usr/bin/python

__Author__ = 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__Date__ = 'Nov 2017'

""" Script which runs fmr.R and prints if the script successfully ran
"""

#importing
import subprocess

#opening the R script and running it in python and setting it to an object
p = subprocess.Popen("/usr/lib/R/bin/Rscript --verbose fmr.R",
shell=True).wait()

#printing if the script ran successfully
if p == 0:
	print "Ran Successfully! Yay!"
else:
	 print "You failed! Boo!"
