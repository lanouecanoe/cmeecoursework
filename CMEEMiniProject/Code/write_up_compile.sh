#!/bin/bash

#Author: Julia Lanoue (j.lanoue17@imperial.ac.uk)
#Date: Feb 2018
#Description: Script which compiles mini-project writeup in LaTex and removes auxiliary files created from LaTex and BibTex

#Creating the pdf from the .tex file
pdflatex write_up.tex
#adding the BibTex file for citations
bibtex write_up
#extra runs to compile the references correctly
pdflatex write_up.tex
pdflatex write_up.tex
#placing the final pdf in the results folder
mv write_up.pdf ../Results

#Removing auxiliary files
rm *.aux
rm *.log
rm *.bbl
rm *.blg