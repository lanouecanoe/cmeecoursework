#!/usr/bin/python

"""A python script containing 5 different functions which print 'hello'
and a series of fooXX functions """

__author__ = 'Julia Lanoue (j.lanoue@imperial.ac.uk)'
__date__ = 'Oct 2017'

import sys 

# 1) prints 'hello' for each number in range
for i in range (3,17):
	print 'hello'
	
#2) prints 'hello' for each number divisible by 3 within range x
for j in range(12):
	if j % 3 == 0:
		print 'hello'
		
# 3) prints 'hello' for each number when by 5 or 4 equals 3 within range x
for j in range(15):
	if j % 5 == 3:
		print 'hello'
	elif j % 4 == 3:
		print 'hello'
	
# 4) prints 'hello' until z = 15
z = 0
while z != 15:
	print 'hello'
	z = z + 3
	
# 5) prints one 'hello' when z = 18 and seven 'hello's when z = 31
z = 12
while z < 100: 
	if z == 31:
		for k in range (7):
			print 'hello'
	elif z == 18:
		print 'hello'
	z = z + 1
	
# fooXX 
def foo1 (x): # multiply x by 0.5
	return x ** 0.5
	
def foo2 (x,y): #print x if x is greater than y
	if x > y:
		return x
	return y
	
def foo3 (x,y,z): #rearranges x,y,z based on relative values
	if x > y:
		tmp = y
		y = x
		x = tmp 
	if y > z:
		tmp = z
		z = y 
		y = tmp
	return [x,y,z]
	
def foo4 (x): #factorial
	result = 1 
	for i in range (1, x + 1):
		result = result * i
	return result

def foo5 (x): #factorial 
	if x == 1:
		return 1
	return x * foo5(x - 1)
	
def main(argv): #assigning values to the main arguments
	print foo1(5)
	print foo2(5,6)
	print foo3(5,6,7)
	print foo4(5)
	print foo5(10)
	
if (__name__ == "__main__"):
	status = main(sys.argv)
	sys.exit (status)
	

