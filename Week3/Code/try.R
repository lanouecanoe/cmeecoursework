#Author: Julia Lanoue (j.lanoue17@imperial.ac.uk)
#Date: Oct 2017
#Description: An R script using the try() function to check for errors

x <- rnorm(50) #defining x as 50 random values from the normal distribution

#defining a function where x is a random assortment of the original x vector and numbers can be repeated. If there are more than 30 unique values of x, print the mean value
doit <- function(x){
  x <- sample(x, replace = T)
  if(length(unique(x))>30) {
    print(paste("Mean of this sample was:", as.character(mean(x))))
  }
}

try(doit)

#applying the function doit to 100 variables
result1 <- lapply(1:100, function(i) try(doit(x)))

#applying the function using a for loop instead of an apply function
result2 <- vector("list",100)
for(i in 1:100){
  result2[[i]] <- try(doit(x))
}
