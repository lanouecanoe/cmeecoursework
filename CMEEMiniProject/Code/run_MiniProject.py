#!/usr/bin/python

#Author: Julia Lanoue (j.lanoue17@imperial.ac.uk)
#Date: March 2018

"""Script which runs the entire mini project - the fitting python script with the data modification R script subprocessed within it, the image R script which does all the plotting for the project, and the bash script which compiles the final report from LaTex"""

#importing packages
import subprocess
import time
#setting time to determine how long each section takes to run
start = time.time()
#running the data modification script and the fitting script
import fitting
fitTime = time.time() - start

#running R script which creates all the images for the write up as well as plots all the curves for each group
start = time.time()
x = subprocess.Popen("/usr/lib/R/bin/Rscript --verbose plot_building.R",
    shell=True).wait()
plotTime = time.time() - start

#printing if the scripts ran successfully
if x == 0:
    print "Plotting completed"
else:
	print "The plots have plopped"

#running the bash script which compiles the LaTex document
# opening the bash script as a text file then reading it into subprocess - avoids permission errors
start = time.time()
with open('write_up_compile.sh', 'rb') as file:
    script = file.read()
    y = subprocess.call(script, shell=True)
    compileTime = time.time() - start

#printing if the script works
if y == 0:
	print "Mini project run completed"
else:
	print "My LaTex is sticking"

totalTime = fitTime + plotTime + compileTime
#printing total run time for the mini project and percentage breakdowns for the different components
print "Mini project run time:{0} minutes".format(round(totalTime/60,3))
print "Time percentages:"
print "Data modification and fitting:{0} %".format(round(fitTime/totalTime*100,3))
print "Plotting and image building:{0} %".format(round(plotTime/totalTime*100,3))
print "LaTex compiling:{0} %".format(round(compileTime/totalTime*100,3)) 