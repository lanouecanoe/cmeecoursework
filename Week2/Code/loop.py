#!/usr/bin/python

"""A python script providing examples of 'for' loops"""

__author__ = 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__date__ = 'Oct 2017'

# for loops
for i in range(5): #print i within the given range
	print i 

my_list = [0,2,"geranimo!",3.0,True,False]
for k in my_list: #printing each value in the above list
	print k
	
total = 0 
summands = [0,1,11,111,1111]
for s in summands: #printing the summation of the total with each item in the above
	print total + s
	
z = 0
while z < 100: #printing values of z while z is under 100
	z = z + 1
	print (z)
	
b = True 
while b: #printing the statement below as long as the statement above is true
	print "GERONIMO! infinite loop! ctrl+c to stop"
	

