#!/usr/bin/python

#Author: Julia Lanoue (j.lanoue17@imperial.ac.uk)
#Date: Jan 2018
#Last modified: March 2018

"""
    Python script which runs an R script used to subset and modified TPC data and performs an NLLS analysis. The script outputs a new csv which contains the minimized parameters, statistial values from the NLLS, along with the inital and measured values
"""

#importing packages
import subprocess
import pandas as pd
import numpy as np
import numpy.polynomial.polynomial as poly
from lmfit import minimize, Parameters, report_fit
# import matplotlib.pyplot as plt

#subprocess used to run the R script which subsets and prepares the raw data for model fitting
p = subprocess.Popen("/usr/lib/R/bin/Rscript --verbose modify_data.R",
    shell=True).wait()

#printing if the script ran successfully
if p == 0:
	print "Raw data has been modified and imported - fitting about to begin!"
else:
	print "Dude where's my data?!"

#importing data as pandas dataframe
data = pd.read_csv('../Data/modified_data.csv', sep = ",")

#functions used in script
def rsquared(expected, y):
    """Function which inputs the expected y-values and the observed y-values and calcuated the R-sqaured values
    """
    rss = np.sum((y - expected)**2) 
    tss = np.sum((y - np.mean(y))**2)
    rsq = 1-(rss/tss)
    return round(rsq, 3)

def get_aic(expected, y):
    """Function which calculates the AIC value for the cubic model"""
    n = len(y) #defining the sample size
    p = 4 #defining number of free parameters 
    rss = np.sum((y - expected)**2) #caluating rss 
    aic = n*np.log((2*np.pi)/n)+n+2+n*np.log(rss)+2*p #calculating aic
    return aic

###### Cubic Model Fitting ######
def cubic_fit(data):
    """Function which subsets the data, performs nlls using the cubic model and outputs the R squared, residual sum of squares, and minimized parameters """
    #subsetting the data for each group
    subset = data
    temp = subset['ConTemp'].values + 273.15
    bval = subset['OriginalTraitValue'].values
    #getting minimized parameters using polyfit
    params = poly.polyfit(temp,bval,3,full=False)
    #calculating expected y-values with the minimized parameters
    expected = poly.polyval(temp,params)
    #calculating r-squared, residual sum of squares, and aic for the model fits
    rsq = rsquared(expected, bval)
    rss = np.sum((bval - expected)**2)
    aic = get_aic(expected, bval)
    # plt.plot(temp, expected)
    # plt.plot(temp, bval, marker = "o")
    # plt.show()
    return rsq, rss, aic, params

# cubic_fit(data.loc[data.NewID == 5])

##### Boltzmann Arrhenius #####
def BA(params, T,stv):
    "Function which describes Boltzmann-Arrhenius equation and out puts the residual values between the calculated values and the measured trait values"
    #defining parameters and constants
    B0 = params['B0']
    E = params['E']
    k = 8.167*10**-5
    #Boltzmann-Arrhenius equation
    expected = B0*np.exp((-E/k)*(1/T - 1/283.15))
    #logged Boltzmann-Arrhenius equation
    # expected = np.log(B0)-(E/k)*(1/T - 1/283.15)
    return expected - stv 

# BA(params, temp, bval)

def BA_fit(data):
    """Function which subsets the data, defines parameters, performs the nlls and outputs the minimized parameter values for the Boltzmann-Arrhenius
    """
    subset = data
    #Subsetting data
    temp = subset['ConTemp'].values + 273.15
    bval = subset['OriginalTraitValue'].values
    #Setting initial values for parameters
    params = Parameters()
    params.add('B0', value = subset['B_Ref'].values[0], min=0)
    params.add('E', value = subset['E'].values[0], min = 0, max = 5)
    #Calculating parameters which minimize residuals
    output = minimize(BA, params, args=(temp, bval))
    #Calculating expected y-values with minimized parameters, r-squared, and residual sum of squares
    yhat = BA(output.params,temp, bval) + bval
    rss = output.residual.var()
    rsq = rsquared(yhat, bval)
    # plt.plot(temp,bval, marker = "o")
    # plt.plot(temp,yhat, 'r')
    # plt.show()
    return output.aic,rss, rsq, output.params

# BA_fit(data.loc[data.NewID == 111])

##### Schoolfield Model #####
def schoolfield(params, T, stv):
    """Function which describes the Schoolfield model, sets parameters for B reference, activation energy, activation energy at high T and T_peak and outputs the residuals between calculated and measured trait values"""
    #Defining parameters and constants
    B0 = params['B0']
    E = params['E']
    E_h = params['E_h']
    E_L = params['E_L']
    T_peak = params['T_peak']
    T_min = params['T_min']
    k = 8.167*10**-5
    #Full Schoolfield equation
    expected = (B0*np.exp((-E/k)*(1/T - 1/283.15)))/(1+np.exp((E_L/k)*(1/T_min - 1/T))+np.exp(E_h/k*(1/T_peak - 1/T)))
    #logged simplified Schoolfield equation
    #expected = np.log(B0)-((E/k)*(1/T - 1/283.15))-np.log(1+np.exp(E_h/k*(1/T_peak - 1/T)))
    return expected - stv

# schoolfield(params, temp, bval)

def SF_fit(data):
    """Function which subsets the data, defines parameters, performs the nlls and outputs the minimized parameter values for the Schoolfield model"""
    subset = data
    #Subsetting data
    temp = subset['ConTemp'].values + 273.15
    bval = subset['OriginalTraitValue'].values
    #Adding inital values for parameters
    params = Parameters()
    params.add('B0', value = subset['B_Ref'].values[0], min=0)
    params.add('E', value = subset['E'].values[0], min = subset['E_low'].values[0], max = subset['E_high'].values[0])
    params.add('E_h', value = subset['E_high'].values[0], min = subset['E'].values[0], max = 12)
    params.add('E_L', value = subset['E_low'].values[0], min = 0, max = subset['E'].values[0])
    params.add('T_peak', value = subset['T_peak'].values[0] + 273.15, min=min(temp), max=max(temp))
    params.add('T_min', value=subset['T_min'].values[0] + 273.15, max = subset['T_peak'].values[0] + 273.15)
    output = minimize(schoolfield, params, args = (temp, bval))
    yhat = schoolfield(output.params,temp, bval) + bval
    rss = output.residual.var()
    rsq = rsquared(yhat, bval)
    # plt.plot(temp,bval, marker = "o")
    # plt.plot(temp,yhat, 'r')
    # plt.show()
    return output.aic, rss, rsq, output.params

# SF_fit(data.loc[data.NewID == 210])

def compare(data):
    """Function which inputs a dataframe, chooses the lowest value from each row and adds a column which contains the column name from which the lowest value came"""
    #getting arrays for each row of two lowest AIC values
    sort = np.sort(data.values)[:,:2]
    #taking the absolute value of the difference between the two lowest AIC values
    data['Difference'] = abs(np.diff(sort))
    #adding the model name for the lowest AIC value if the difference between the two lowest was over 2
    data['Best Model'] = np.where(data['Difference']>2,data.loc[:,['BA_AIC','Cubic_AIC','SF_AIC']].T.idxmin(skipna=True),'Shared')
    #Returning a count the number of times each model had the lowest AIC
    return data['Best Model'].value_counts()

# weighted AIC values
# def better(data):
#     data = data.sub(data.min(axis = 1), axis = 0)
#     multiply = lambda x: np.exp(-0.5*x)
#     data = data.apply(multiply)
#     data = data.div(data.sum(axis=1), axis=0)
#     data['Best Model'] = data.T.idxmax()
#     return data['Best Model'].value_counts()
# better(test)

# determining the lowest AIC for values within the 'Shared' category
# test = DataAIC.loc[DataAIC['Best Model'] == 'Shared']
# test = test.drop(['Difference','Best Model'], axis = 1)
# test['min'] = test.loc[:,['BA_AIC','SF_AIC','Cubic_AIC']].T.idxmin()
# test['min'].value_counts() 

#creating empty lists
cubic_rss = []
cubic_rsq = []
cubic_aic = []
cubic_B0 = []
cubic_B1 = []
cubic_B2 = []
cubic_B3 = []
ba_aic = []
ba_rss = []
ba_rsq = []
ba_B0 = []
ba_E = []
sf_aic = []
sf_rss = []
sf_rsq = []
sf_B0 = []
sf_E = []
sf_Eh = []
sf_Tpeak = []
sf_EL = []
sf_Tmin = []

#for loop to run the NLLS
for i in data.NewID.unique():
    try:
        #running cubic model and saving output
        cubic_results = cubic_fit(data = data.loc[data.NewID == i])
        #subetting the cubic model output to seperate lists
        cubic_rsq.append(cubic_results[0])
        cubic_rss.append(cubic_results[1])
        cubic_aic.append(cubic_results[2])
        cubic_B0.append(cubic_results[3][0])
        cubic_B1.append(cubic_results[3][1])
        cubic_B2.append(cubic_results[3][2])
        cubic_B3.append(cubic_results[3][3])
    except ValueError:
        #Adding NA's to lists if any of the curves do not converge
        cubic_rsq.append(np.NaN)
        cubic_rss.append(np.NaN)
        cubic_B0.append(np.NaN)
        cubic_B1.append(np.NAN)
        cubic_B2.append(np.NaN)
        cubic_B3.append(np.NaN)
    try:
        #running Boltzmann Arrhenius model and saving output
        ba_results = BA_fit(data = data.loc[data.NewID == i])
        #subsetting output to empty lists
        ba_aic.append(ba_results[0])
        ba_rss.append(ba_results[1])
        ba_rsq.append(ba_results[2])
        ba_B0.append(ba_results[3].values()[0].value)
        ba_E.append(ba_results[3].values()[1].value)
    except ValueError:
        #Adding NA's to lists for curves that don't converge
        ba_aic.append(np.NaN)
        ba_rss.append(np.NaN)
        ba_rsq.append(np.NaN)
        ba_B0.append(np.NaN)
        ba_E.append(np.NaN)
    try:
       #running Schoolfield model and saving output
        sf_results = SF_fit(data = data.loc[data.NewID == i])
        #Subsetting results to seperate lists
        sf_aic.append(sf_results[0])
        sf_rss.append(sf_results[1])
        sf_rsq.append(sf_results[2])
        sf_B0.append(sf_results[3].values()[0].value)
        sf_E.append(sf_results[3].values()[1].value)
        sf_Eh.append(sf_results[3].values()[2].value)
        sf_EL.append(sf_results[3].values()[3].value)
        sf_Tpeak.append(sf_results[3].values()[4].value)
        sf_Tmin.append(sf_results[3].values()[5].value)
    except ValueError:
        #Adding NA's to lists for curves that don't converge
        sf_aic.append(np.NaN)
        sf_rss.append(np.NaN)
        sf_rsq.append(np.NaN)
        sf_B0.append(np.NaN)
        sf_E.append(np.NaN)
        sf_Eh.append(np.NaN)
        sf_EL.append(np.NaN)
        sf_Tpeak.append(np.NaN)
        sf_Tmin.append(np.NaN)


#Creating dataframe with statistics
DataStats = pd.DataFrame({'NewID':data.NewID.unique(),'BA_RSS':ba_rss,'BA_Rsq':ba_rsq,'Cubic_RSS':cubic_rss,'Cubic_Rsq':cubic_rsq,'SF_RSS':sf_rss, 'SF_Rsq':sf_rsq})
DataAIC = pd.DataFrame({'NewID':data.NewID.unique(),'BA_AIC':ba_aic,'Cubic_AIC':cubic_aic,'SF_AIC':sf_aic}, dtype=float)
#Setting the NewID number has the dataframe index
DataAIC = DataAIC.set_index('NewID')
#Creating dataframe with minimized parameters
DataParams = pd.DataFrame({'NewID':data.NewID.unique(),'C_B0':cubic_B0, 'C_B1':cubic_B1,'C_B2':cubic_B2,'C_B3':cubic_B3,'BA_B0': ba_B0, 'BA_E':ba_E,'SF_B0':sf_B0,'SF_E':sf_E, 'SF_Eh':sf_Eh,'SF_Tpeak':sf_Tpeak, 'SF_EL':sf_EL, 'SF_Tmin':sf_Tmin})
#Merging the two dataframes together then appending the merged dataframe to the original data
newdata = pd.merge(DataParams,DataStats,on='NewID')
newdata = pd.merge(data, newdata, on = 'NewID')
#Creating csv file with data to plot fits
newdata.to_csv('../Data/data4plotting.csv')

print "Fitting completed, data saved to csv"

#Printing the number of curves for which each model had the lowest AIC
print "Number of best fits for each model:"
print compare(DataAIC)
