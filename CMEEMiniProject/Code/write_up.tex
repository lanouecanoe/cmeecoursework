\documentclass[11pt]{article}
\usepackage[margin=2cm]{geometry}
\usepackage{lineno}
\usepackage{indentfirst}
\usepackage{siunitx}
\usepackage{textcomp}
\usepackage{multirow}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{setspace}

\begin{document}
	\begin{titlepage}
		\vspace*{\fill}
		\begin{center}
			\LARGE \underline{Computational Methods in Ecology \& Evolution Mini Project:} \\
			\vspace{5mm}
			\textbf{\LARGE{Mechanistic vs Phenomenological:\\
					Model Selection and Optimal Fitting of Thermal Performance Curves}} \\
			\title{\line(1,0){250}\\A Title\\\line(1,0){250}}
			\vspace{2mm}
			\Large{Julia Lanoue} \\
			\emph{Department of Life Sciences, Silwood Park, Imperial College London} \\
			March 9th 2018 \\
			\vfill
			Word Count: 2993
		\end{center}
	\end{titlepage}
	
\tableofcontents
\vfill

\linenumbers
\doublespacing
	\section{Introduction}
	Temperature is a fundamental abiotic factor in many biological processes, ranging from cellular metabolism to trophic interactions \cite{Brown2004};\cite{Schulte2015}. Over the past decades, metabolic theory has sought to explain the influence of temperature on metabolism and its overarching effects on community dynamics \cite{Brown2004};\cite{Dell2011};\cite{OConnor2011}. One such theory, the Metabolic Theory of Ecology (MTE), states that metabolic rate varies across temperature because the rate of biochemical reactions is temperature-dependent. This is usually expressed in terms of activation energies, which is defined as the energy required for a reaction to take place \cite{Gillooly2001}. This range of metabolic rates is illustrated as a thermal performance curve. Thermal performance curves (TPC) are described as unimodal with a rise, fall, and transition phase \cite{Dell2011};\cite{Schulte2015}. The aim of the MTE is to ascribe a mechanistic model to observed thermal performances \cite{Brown2004}. Various equations, both phenomenological and mechanistic, have been used to describe them.
	\par Two of the most prominent equations in metabolic theory are the Boltzmann-Arrhenius and the Schoolfield models. The Boltzmann-Arrhenius model is
	\begin{equation}
	B = B_0e^{\frac{-E}{k}(\frac{1}{T} - \frac{1}{283.15})}
	\end{equation}
	B\textsubscript{0} is the metabolic rate at a reference temperature, E is the activation energy measured in electron-volts (eV), k is Boltzmann's constant (\SI{8.167e-5}{\eV\per\kelvin}), T is the temperature in Kelvin. Though this model does provide good estimates for metabolic rates within the "biologically relevant temperature range" \cite{Gillooly2001}, the linear nature of the Boltzmann-Arrhenius model poorly describes the unimodal shape of the TPC. It can adequately model the rising phase but fails to account for the peak and ultimate decline in thermal performance \cite{DeLong2017}. Model estimates for parameters, such as activation energy, can also become skewed depending on the measured temperature range and the level of nonlinearity in the data \cite{Pawar2016}. 
	\par The Sharpe-Schoolfield model is an extension of the classic Boltzmann-Arrhenius which aims to better explain the peak and falling phase of the curve by assuming that the activation energy is not constant across temperature. The model includes a low and high activation energy (E\textsubscript{l} and E\textsubscript{h}, respectively) as well as low and high temperature bounds. The model is written as
	\begin{equation}
	B = \frac{B_0e^{\frac{-E}{k}(\frac{1}{T} - \frac{1}{283.15})}}{1+e^{\frac{E_l}{k}(\frac{1}{T_l} - \frac{1}{T})}+e^{\frac{E_h}{k}(\frac{1}{T_h} - \frac{1}{T})}}
	\end{equation}
	These additions follow the assumption that activation energy is higher at high temperatures and lower at low temperatures \cite{Schoolfield1981}. The non-linear nature of the Schoolfield model, in theory, should be better suited to describe the unimodal shape than the Boltzmann-Arrhenius model because all three regions are explicitly accounted for in the model. 
	\par Throughout the biological sciences, there has been a recent push to use model selection techniques over null hypothesis approaches \cite{Johnson2004}. Traditionally, models are compared against a null hypothesis, where evidence for or against a model is based on an arbitrarily chosen p-value. This technique hinders development of better biological theory because inferences cannot be drawn from the results, and the method is not grounded in biological principles \cite{Johnson2004}. Model selection, on the other hand, is based on maximum likelihood theory, which provides a more robust framework for model comparison. In this technique, the Akaike Information Criteria (AIC) or Baysian Information Criteria (BIC) is used to determine the preferred model. Both are based on information theory, which measures the amount of information lost from each model and also takes into account the number of parameters, thereby adhering to the principle of parsimony \cite{Burnham2004}; \cite{Johnson2004}.
	\par In this project, I use model selection to determine the preferred model for fitting thermal performance curves to measured data and compare model results for parameter estimates of activation energy and B\textsubscript{0}. In addition to the Boltzmann-Arrhenius and Schoolfield models, I also compare a standard cubic equation:
	\begin{equation} 
	B = B_0 + B_1T + B_2T^2 + B_3T^3
	\end{equation}
	The cubic model has no biological underpinnings, and is,therefore, considered to be a phenomenological model. In this context, the various B values represent metabolic rates and T is temperature in Celsius. This model was chosen because visually, a thermal performance curve seems to follow a cubic pattern. Because mechanistic models usually provide better estimates than phenomenological ones \cite{AngilletaJr.2009}, I predict that through this analysis, the mechanistic models will better describe thermal performance curves, and the mechanistic models will also provide closer parameter estimates to the measured data.  
	  
	\section{Methods}
	For this analysis, I worked with a subset of the larger BioTraits Database, which is a compilation of experimental thermal response data. The organisms within the dataset span nine Kingdoms and over thirty Phyla, ranging from Archaea to Fungi. Only data on consumers was used in the analysis. 
	  \subsection{Missing Values} 
	  Any missing temperature values for the organism were replaced with a corresponding measurement for environmental temperature. It is possible to make this replacement because oftentimes, an ectotherm's body temperature is influenced by its surrounding temperature \cite{Brown2004}. If no corresponding value was available, the row of data was removed. The original trait values were used for the analysis. Any missing, negative, or zero values were removed. After missing values were sorted, groups which had five measurements or less were removed from the dataset because over five data points are needed to create a thermal performance curve. During the analysis, it was also noted that four groups within the dataset only contained measurements for a single temperature which led to groups with less than 2 unique temperature measurements also being excluded. The remaining groups were then assigned new ID numbers to more easily identify and keep track of the number of groups throughout the analysis. 
	  \subsection{Starting Values}
	  Starting values for several variables were calculated from the measured data, including reference trait value, peak temperature, and activation energy. The reference trait value (B\textsubscript{0}) is the trait value at the reference temperature, which I defined as 10\textdegree C. If 10\textdegree C was not measured in a group, I set B\textsubscript{0} to the measured trait value at the temperature closest to the reference. The peak temperature is defined as the temperature at which the trait value is at its maximum; the starting value was found by finding the temperature at that maximum value. 
	  \par The activation energy (E) was found by performing a linear regression on the logged trait values (B) over 1/kT, where, k is Boltzmann's constant and T is the temperature in Kelvin. The slope from this technique provides an approximation for the activation energy (Figure 1). To perform the linear regression, the data was further subsetted to only include values greater than the peak temperature. The slope from the peak temperature point to the maximum temperature was taken for each group. If there were not enough points in the group greater than T\textsubscript{peak} for the regression to take place, the activation energy was set to 0.65 eV, which is often cited as the average energy for respiration \cite{Gillooly2001};\cite{Brown2004}. The maximum activation energy was set to three eV. If the calculated E value was over three, it was adjusted to be the maximum value. This was done to keep the activation energy within a biologically plausible range \cite{Gillooly2001}. I assume that the high and low activation energies vary from the calculated estimate by a factor of four. Those starting values were estimated by multiplying and dividing the calculated value by that factor.
	  \begin{figure}
	  	\centerline{\includegraphics[height=7cm]{../Results/Images/SlopeExample.png}}
	  	\caption{Graph providing a visual representation of the linear regression done to estimate starting values for activation energies. The dashed vertical line represents the peak trait value (T\textsubscript{peak}). The regression was performed on all 1/kT values greater than or equal to T\textsubscript{peak}. The red line shows the regression line. The slope, which in this example is 0.354, is used as the activation energy estimate.}
	  	\label{Figure 1}
	  \end{figure}
	  \subsection{Non-linear Least Squares Fitting and Model Selection}
	  For the Boltzmann-Arrhenius equation, bounds were placed on values for B\textsubscript{0} and E. This was done to ensure that values for those parameters stay within a biologically meaningful range. The minimum value for B\textsubscript{0} was set to zero, and the values for E could range from zero to three eV. Three eV was set to the maximum to keep with the maximum set during the calculation of the starting values. 
	  \par The Schoolfield model required additional bounds. The same bounds were placed on B\textsubscript{0}. Maximum and minimum bounds were added to E, where the maximum value is the calculated E\textsubscript{h} value and the minimum is the E\textsubscript{l} value. The minimum for E\textsubscript{h} was set to the initial value for E and the maximum was set to 12 eV, also keeping in line with the starting value calculation boundaries. For E\textsubscript{l}, the minimum boundary was set to zero and the maximum was set to the calculated E value. Setting these boundaries prevents overlaps when parameters are calculated. Boundaries for T\textsubscript{peak} were set to the minimum and maximum measured temperature values for each group. This was done to ensure that the temperature values did not stray outside the range of the measured temperatures.
	  \par Part of the NLLS fitting is calculating statistics to determine the goodness of fit for each model. To determine this, I calculated the Akaike information criterion (AIC) and R\textsuperscript{2} for each curve. R\textsuperscript{2} values greater than or equal to 0.6 were counted as a good fit. The AIC value is then used for model comparison, whereby the model with the lowest AIC value is best suited for the data. The AIC was calculated through lmfit for the Boltzmann-Arrhenius and the Schoolfield model and manually calculated for the cubic model. A function was then used to compared the AIC values for each model and returned which model had the lowest value for each group, given that the difference between the lowest and second lowest AIC values are greater than 2 \cite{Burnham2004}.        
	  \subsection{Computing Languages}
		\subsubsection*{R}
		R is used in three scripts - one to subset the raw data, remove any missing values, calculate starting values from the data and the other two are used to plot all the graphs. R was used because packages, such as Dplyr, allow for easy subsetting and calculations while avoiding for loops, and packages such as ggplot2 are easy ways to create high quality figures \cite{RDevelopmentCoreTeam2008}. 
		\subsubsection*{Python}
		Python was used to run the NLLS as well as calculate any statistical figures used in later analyses, such as R\textsuperscript{2} values. Available packages, such as lmfit and NumPy, make for easier calculations, and packages like Pandas allow for quick dataframe creation and manipulation \cite{PythonSoftwareFoundation2013}; \cite{Newville2014}. 
		\subsubsection*{Bash}
		Bash was used to compile the LaTex script, incorporate BibTex, and remove auxiliary files from the compiling process. 
	
	\section{Results}
		\subsection{Parameter Values}
		I categorized the measured response value for each curve into one of three groups: photosynthesis, respiration, or growth rate. I compared parameter estimates outputted from the NLLS for activation energy (E) and the reference response value (B\textsubscript{0}) since both the Boltzmann-Arrhenius and the Schoolfield incorporate those parameters. 
		\subsubsection*{Activation Energy}
		It is often stated in the literature that E falls between 0.6-0.7 eV \cite{Gillooly2001}; \cite{Brown2004}; \cite{Dell2011}. I found, based on the initial linear regression results, photosynthesis rates have a lower activation energy than respiration and growth rates. This pattern was maintained even after the parameters were minimized (Figure 2, Table 1). Growth rate has the highest activation energy of all the groups. The Schoofield model also produced the highest activation energy values and had the highest variability. The mean E for all the groups was 0.6 eV while the median value was 0.5 eV (Figure 3).
		\begin{table}[h!]
			\centering
			\begin{tabular}{||c| c | c | c | c | c ||}
			\hline
			Trait Type & Number of Groups & Model Type & Mean (eV) & Variance (eV) & Median (eV) \\
			\hline
			 \multirow{3}{*}{Growth Rate} & 
				\multirow{3}{*}{733} & Initial Value & 0.800 & 0.203 & 0.736 \\
				& & Boltzmann-Arrhenius & 0.384 & 0.188 & 0.223 \\
				& & Schoolfield & 1.10 & 0.655 & 1.036 \\
			\hline
			 \multirow{3}{*}{Photosynthesis}& 
				\multirow{3}{*}{504} & Initial Value & 0.432 & 0.139 & 0.347 \\
				& & Boltzmann-Arrhenius & 0.091 & 0.013 & 0.049 \\
				& & Schoolfield & 0.748 & 0.349 & 0.638 \\
			\hline
			\multirow{3}{*}{Respiration} & 
				\multirow{3}{*}{344} & Initial Value & 0.508 & 0.085 & 0.486 \\
				& & Boltzmann-Arrhenius & 0.372 & 0.042 & 0.363 \\
				& & Schoolfield & 0.782 & 0.310 & 0.689 \\
			\hline
		\end{tabular}
		\caption{The mean, variance, and median values of the activation energies comparing the initial values and the minimized parameters estimated through the NLLS for the Schoolfield and Boltzmann-Arrhenius models. The activation energies are split into three categories, growth rate, photosynthesis, and respiration, based on their standardized trait names. There are 733 data groups categorized under growth rate, 504 groups in photosynthesis, and 344 in respiration. The number of groups within each category may contribute to the amount of variation seen in the measurements.}
		\label{Table 1}
	\end{table}
		
		\begin{figure}
			\centerline{\includegraphics[width = 7cm, height=6cm]{../Results/Images/ECompare.png}}
			\caption{Visual representation of the range of activation energies between the three models and three trait types. The activation energy scale represented here is limited to five eV for ease of viewing. Photosynthesis rates have the lowest average activation energy, and the Schoolfield model produced the highest average rates and also has the highest variance.}
			\label{Figure 2}
		\end{figure}	
		
		\begin{figure}
			\centerline{\includegraphics[height=7cm]{../Results/Images/Ehistogram.png}}
			\caption{Histogram representing the distribution of activation energies across trait types. The maximum activation energy was limited to 5 eV for easier visualization, though the full set was used in statistical calculations. The solid red line represents 0.65 eV, which was used as the starting value for groups with less than 2 data points for the regression. The dotted line shows the mean (0.6 eV) and the dashed line represents the median (0.5 eV).}
			\label{Figure 3}
		\end{figure}
		
		\subsubsection*{Reference Response Value}
		Because the original response values were used during the analyses instead of the standardized values, there is a large amount of variability in the measured units. This is reflected in Figure 4, which shows the range of reference values collected from the data. Reference values estimated from the Boltzmann-Arrhenius and Schoolfield show the same distribution of values. The cubic model, however, shows a completely different distribution, with values two to three orders of magnitude larger than the measured data (Table 2).
		
		\begin{table}
			\centering
			\begin{tabular}{||c | c | c | c | c ||}
				\hline
				Trait Type & Model & Mean & Variance & Median\\
				\hline
				\multirow{4}{*}{Growth Rate} & Measured & 5.980 & \num{5.478e2} & \num{5.058e-02} \\
				& Cubic & 7496 & \num{2.448e10} & \num{6.033e2} \\
				& Boltzmann-Arrhenius & 2.788 & \num{1.028e2} & \num{7.887e-2}\\
				& Schoolfield & 14.21 & \num{5.836e3} & \num{8.894e-2} \\
				\hline
				\multirow{4}{*}{Photosynthesis} & Measured & 31.71 & \num{3.669e13} & 8.184 \\
				& Cubic & \num{4.144e5} & \num{1.687e13} & \num{2.985e3} \\
				& Boltzmann-Arrhenius & 47.309 & \num{6.473e4} & 8.351 \\
				& Schoolfield & 58.082 & \num{6.734e4} & 14.44 \\
				\hline
				\multirow{4}{*}{Respiration} & Measured & 16.14 & \num{4.418e3} & 1.339 \\
				& Cubic & \num{-1.814e4} & \num{1.170e13} & \num{8.683e2} \\
				& Boltzmann-Arrhenius & 14.37 & \num{3.015e3} & 1.408 \\
				& Schoolfield & 43.608 & \num{3.982e4} & 3.781\\
				\hline
			\end{tabular}
			\caption{Table displaying the mean, variance, and median values for the reference response values. The  values predicted by the Boltzmann-Arrhenius model were closest to the measured values, while the cubic model predictions were the furthest. The high variability is likely a result of the original response values being used in the analysis instead of the standardized values because the units are not standardized.}
			\label{Table 2}
		\end{table}
		
		\begin{figure}
			\centering
			\subfigure[]{
				\includegraphics[height=6cm]{../Results/Images/bref.png}}
			\subfigure[]{
				\includegraphics[height=6cm]{../Results/Images/baref.png}}
			\subfigure[]{
				\includegraphics[height=6cm]{../Results/Images/sfref.png}}
			\subfigure[]{
				\includegraphics[height=6cm]{../Results/Images/cubicref.png}}
			\caption{Figure comparing the distribution of reference response values across models. Dashed lines represent the mean and dotted lines represent the median. For visualization, the y-axis measurements range from zero to 200 for all graphs except the cubic. The response value units are not labeled as units vary by group as a result of the original trait values being used. (a) The reference values collected from the measured data. (b) Reference values estimated for the Boltzmann-Arrhenius equation. (c) Reference values estimated  for the Schoolfield model. (d) Reference values estimated for the cubic. Note the maximum value on the x-axis is 30, instead of 200 and the y-axis ranges from -100 to 200. The mean and median values are beyond the scope of the graph but can be seen in Table 2.}
			\label{Figure 4}
		\end{figure}   
		
		\subsection{Model Selection}
		According to the model selection analysis,the Schoolfield model is best suited for modeling thermal performance curves. Out of the 1582 curves used in the analysis, the Schoolfield proved to have the lowest AIC for over 60\% of the curves. The Boltzmann-Arrhenius was the preferred model for 30\%, and the cubic model was preferred for less than 3\% of the curves. The remaining 7\% were equally well suited for at least two of the models (Table 2). The Schoolfield model most accurately described curves which display the classic unimodal characteristics of the thermal performance curve, where a defined rise, peak, and decline are seen (Figure 5a). The Boltzmann-Arrhenius was best suited for curves where only the rising phase of the was recorded and therefore explicitly show the linear relationship which the model describes (Figure 5b). For curves in which the cubic model was preferred, often the data points did not follow a classic unimodal shape and showed little linearity, resulting in the Schoolfield and the Boltzmann-Arrhenius poorly predicting the shape (Figure 5c).
		\begin{figure}
			\centering
			\subfigure[]{
				\includegraphics[height=5cm]{../Results/FitGraphs/Plot1025.png}}
			\subfigure[]{
				\includegraphics[height=5cm]{../Results/FitGraphs/Plot33.png}}
			\subfigure[]{
				\includegraphics[height=5cm]{../Results/FitGraphs/Plot844.png}}
			\caption{Figures providing examples where each model had the lowest AIC. (a) Plot where the Schoolfield model was the preferred model. A distinct unimodal shape is observed. (b) Plot where the Boltzmann-Arrhenius was the preferred model. Lines for the Schoolfield and the cubic models overlap. The rising phase of the curve was recorded and therefore is well described by the equation. (c) Curve where the cubic model was preferred. Lines for the Boltzmann-Arrhenius and the Schoolfield models curves overlap. The data points do not allude to the unimodal shape of a TPC and does not show the linear nature of the rising phase. Therefore the cubic model provided the best fit for the data.}
			\label{Figure 5}
		\end{figure}
		\par R\textsuperscript{2} values were also calculated as a preliminary analysis for goodness of fit. Out of 1582 curves, the cubic model had the most R\textsuperscript{2} values greater than or equal to 0.6. The Schoolfield model had the next highest number, followed by the Boltzmann-Arrhenius (Table 3).   
		\begin{table}[h!]
			\centering
			\begin{tabular}{||c | c | c||}
			\hline
			Model & Number of Curved as Preferred Model & Number of Curves with R\textsuperscript{2} $\geq$ 0.6 \\ [0.5ex]
			\hline
			Cubic & 45 & 1467 \\
			Boltzmann-Arrhenius & 475 & 612 \\
			Schoolfield & 969 & 1397 \\
			\hline
			\end{tabular}
			\caption{Table showing the model type, the number of curves for which each model was the preferred model in according to the AIC results, and the number of curves for each which had an R\textsuperscript{2} value greater than or equal to 0.6.}
			\label{Table 3}
		\end{table}
		
	\section{Discussion}
	As expected, the Schoolfield model was the preferred model for majority of the curves. This coincides with previous research done on thermal performance curves which state that the simple Boltzmann-Arrhenius does not adequately describe the full shape of the curve \cite{Schulte2015};\cite{DeLong2017}. This is because the Boltzmann-Arrhenius equation assumes a constant reaction rate across temperature gradients. Though this equation is useful when the data does not cover the complete range of the thermal performance curve. The Boltzmann-Arrhenius does describe the rising phase of the curve well, though it does not provide any explanation for the two remaining phases.
	\par In this project, it seems that the Schoolfield model fills the gaps left by the Boltzmann-Arrhenius. By allowing the activation energy to vary with temperature, the Schoolfield model provides further insight into the shape of the thermal response curve. This is important information when investigating the effects of temperature on metabolic reactions in the face of climate change, where the performance individuals and populations is expected to shift in response to increases in average temperature and variability \cite{OConnor2011}; \cite{Vasseur2014}. It is also important to consider that these curves represent acute reactions to changes in temperature and therefore, may not necessarily reflect long-term responses to warming through acclimation or adaptation \cite{Schulte2011}.
	\par For activation energy, the results show photosynthesis rates to have the lowest activation energy and growth rates to have the highest. This is consistent with previous studies which also found rates of photosynthesis to have lower activation energies \cite{Allen2005};\cite{Dell2011}. An analysis of activation energy distribution revealed that over 63\% of activation energies fell below the 0.65 eV threshold. The mean value of the data was 0.6 eV, which is consistent with previous studies \cite{Gillooly2001}. However, the median value was 0.5 eV. This "right-skewness" of the data has been noted in studies such as Dell et al. (2011), which found the activation energy to range from 0.2-1.2 eV for the rise component and 1.31-5.13 eV when averaged overall \cite{Dell2011}. These results give evidence that there is more variation in activation energies across species and traits than previously noted, and more research must be done to further understand this variability. 
	\par It seems paradoxical that the cubic model created the best fits for the largest number of curves, but was the preferred model for only 45 out of 1582 curves. The cubic model is a purely phenomenological model; none of the parameters have any biological meaning. As seen in Equation 3, phenomenological models like the cubic are capable of relating abiotic and biotic factors to make predictions, but it is not possible to make strong inferences from the results. Correlative or phenomenological equations can often lead to inaccurate predictions because significant biological components, such as dispersal or phenotypic plasticity are ignored. The results from these types of models offer no insight into why these patterns are observed \cite{AngilletaJr.2009}. This can especially be seen in Table 2, where B\textsubscript{0} values are compared. Though the cubic model fits the curves well, the parameter values it produced are biologically improbable and for each trait type, often four orders of magnitude larger than the measured value. The strength in the mechanistic framework surrounding the other two models is the ability to make inferences which can be carried across scales. Understanding the mechanistic basis for biological phenomena is a powerful tool when thinking about the long-term effects of environmental shift, such as climate change, and how biological traits may change across space and levels of organization \cite{AngilletaJr.2009};\cite{Schulte2015}. Mechanistic models such as the Schoolfield and the Boltzmann-Arrhenius can guide conservation and mitigation strategies when thinking about climate change and its impacts across levels of organization because of their power to estimate thermal responses and provide biologically relevant results. 
	
	\section{Conclusion}
	In this project, techniques such as non-linear least squares fitting and model selection were used to determine which model best suited thermal performance data. The data measured thermal response values along a temperature gradient to create thermal performance curves, which track how a trait varies in response to temperature. Three models were compared to determine which one best suited the data - a cubic model, the Boltzmann-Arrhenius model, and the Schoolfield model. Through non-linear least squares fitting methods, estimated initial values for each model were minimized, and the preferred model was determined through AIC comparison. The Schoolfield model was found to suit over 60\% of the curves, followed by the Boltzmann-Arrhenius which suited over 30\%. Though the cubic model resulted in a higher number of R\textsuperscript{2} values over the designated threshold, the resulting parameter values did not provide any relevant biological insights. The results of the two mechanistic models provide foundations for further biological inferences, such as how changes in activation energies across temperature affects the rate of metabolic reactions. 
	\par Analysis of activation energy showed that there is greater variation in its bounds than was previously thought. Many papers cite a standard rate of 0.65 eV; however there is evidence that a wider range exists and is at times lower than expected \cite{Gillooly2001}; \cite{Brown2004};\cite{Allen2004};\cite{Dell2011}. The results presented here give further evidence for this wider range, as the median activation energy in this dataset was 0.5 eV (Figure 3). It is also important to note that these measurements only reflect acute temperature responses, and when extrapolating thermal performance to a larger temporal scale factors such as adaptation and acclimation should be taken into account \cite{Schulte2011}. These inferences are possible because of mechanistic models, such as the ones used in this project. The use of biologically-relevant parameters like E and B\textsubscript{0} allow for theories and new discoveries to occur. Model selection can then be used give further weight to these theories. Here, model selection provided further evidence towards the strength of mechanistic models for both predictive power and ability to provide biological insight. 
	
	\newpage
		\bibliographystyle{apalike}
		\bibliography{MiniProj_Papers}
\end{document}	
