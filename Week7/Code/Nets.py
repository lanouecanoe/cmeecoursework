#!/usr/bin/python

__Author__ = 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__Date__ = 'Nov 2017'

"""Script which creates a network graph of the QMEE institution data
"""

#importing packages
import igraph
from igraph import *
import numpy as np
import pandas as pd


#importing data using pandas
edges = pd.read_csv('../Data/QMEE_Net_Mat_edges.csv',sep = ',')
nodes = pd.read_csv('../Data/QMEE_Net_Mat_nodes.csv',sep = ',')

#converting the matrix values into an array
ed = edges.values

#converting the node data into tuples
node_tuple = tuple(map(tuple, nodes.values))
#creating empty list
names = []
#adding the institution names to list
for entry in node_tuple:
	names.append(entry[0])

#creating and adding institution types to a list
types = []
for entry in node_tuple:
	types.append(entry[1])

#Isolating adjacency values 
NonZ = np.where(ed)
#Converting the weights to a list
weights = ed[NonZ].tolist()
#correcting the weight values by dividing each by 4
weights_corrected = [x / 4.0 for x in weights] 

#Creating the graph
g = Graph.Adjacency((ed > 0).tolist()) #creating list of non-zero adjacency values
g.es['edge_curved'] = False #making the edges straight lines
g.vs['label'] = names #adding the labels
g.vs['type'] = types #adding the type of institutions 
g.vs["color"] = ["red", "red", "green", "green", "green", "blue"]  #adding the colors
#creating the plot and saving it with correct values
#legend("top",legend=c("University","Hosting Partner","Non-Hosting Partner"),fill=c("red","green","blue"))
p = plot(g, target = "../Results/Nets.svg", edge_curved = 0, vertex_size = 50, edge_width = weights_corrected)
p.show()
