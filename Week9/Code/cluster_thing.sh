#!/bin/bash

#PBS -l walltime=12:00:00
#PBS -l select=1:ncpus=1:mem=1GB

module load anaconda3/personal
module load intel-suite

echo "R is about to run"

R --vanilla < $WORK/ques18.R >log 2>&1
mv u_betta_work* $WORK

echo "R has finished running"
