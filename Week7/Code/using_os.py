#!/usr/bin/python

__Author__ = 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__Date__ = 'Nov 2017'

""" Script which uses subprocesses to find directories, subdirectories, and files
which start with either upper or lower case C
"""

# Use the subprocess.os module to get a list of files and  directories 
# in your ubuntu home directory 

import subprocess
#~ import re

#################################
#~Get a list of files and 
#~directories in your home/ that start with an uppercase 'C'

home = subprocess.os.path.expanduser("~") #defining home directory
FilesDirsStartingWithC = [] #creating empty list
for (dir, subdir, files) in subprocess.os.walk(home): #for loop to cycle through directories, subdirectories, and files
	#~ attempt at finding the files using regex
	#~ FilesDirsStartingWithC.append(re.findall(r'/[C]\w*/', dir))
	for dirs in subdir: #finding all the C's in subdirectories
		if dirs.startswith('C'): 
			FilesDirsStartingWithC.append(dirs) #adding the names to the empty list
	for things in files: #finding all the C's in the files
		if things.startswith('C'):
			FilesDirsStartingWithC.append(things)
print FilesDirsStartingWithC	
  
#################################
# Get files and directories in your home/ that start with either an 
# upper or lower case 'C'

home = subprocess.os.path.expanduser("~")
Upper_lower = []
for (dir, subdir, files) in subprocess.os.walk(home):
	for dirs in subdir:
		if dirs[0] == 'C' or dirs[0] == 'c': #finding all the upper or lower case C's in subdirectories
			Upper_lower.append(dirs) 
	for things in files:
		if things[0] == 'C' or things[0] == 'c': #finding all upper or lower case C's in files
			Upper_lower.append(things)
print Upper_lower

#################################
# Get only directories in your home/ that start with either an upper or 
#~lower case 'C' 

home = subprocess.os.path.expanduser("~")
dir_C = []
for dir in subprocess.os.walk(home): 
	if dir[0] == 'C' or dir[0] == 'c': #finding all C's in only the home directory
		dir_C.append(dir)
print dir_C
