#!/usr/bin/python

""" A python script which takes a csv file with the species name, order,
distribution, and body mass and creates a new csv file which only contains
the species name and body mass 

Author: Julia Lanoue"""

__author__ = 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__date__  = 'Oct 2017'

import csv  # importing to work with csv files

f= open('../Sandbox/testcsv.csv','rb')

csvread=csv.reader(f) 
temp=[]
for row in csvread: #for loop to create tuples out of each row in file
	temp.append(tuple(row))
	print row
	print "The species is",row[0] 
	
f.close()

f= open('../Sandbox/testcsv.csv','rb')
g= open('../Sandbox/bodymass.csv','wb') #creating the new csv file

csvread= csv.reader(f)
csvwrite= csv.writer(g)
for row in csvread: #for loop adding the species name and body mass to file
	print row
	csvwrite.writerow([row[0],row[4]]) 
	
f.close()
g.close()
