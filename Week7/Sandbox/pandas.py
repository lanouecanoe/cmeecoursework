import pandas as pd
import scipy as sc

MyDF = pd.read_csv('../Data/testcsv.csv', sep = ',')
MyDF

MyDF = pd.DataFrame({
	'col1': ['Var1', 'Var2', 'Var3', 'Var4'],
	'col2': ['Grass', 'Rabbit', 'Fox', 'Wolf'],
	'col3': [1, 2, sc.nan, 4]
})
MyDF

MyDF.values

MyDF.describe()
