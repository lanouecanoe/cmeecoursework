#!/usr/bin/python

"""Excersise demonstrating how to create list comprehensions and 
for loops"""

__author__ = 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__date__ = 'Oct 2017'

taxa = [ 'Quercus robur',
		'Fraxinus excelsior',
		'Pinus sylvestris',
		'Quercus cerris',
		'Quercus petraea',
		]
		
def is_an_oak(name): #defining a function which returns any value that starts with 'quercus'
	return name.lower().startswith('quercus')

oaks_loops = set() #creating an empty set
for species in taxa: #for loop going through taxa set and adding the oaks to the empty set
	if is_an_oak(species):
		oaks_loops.add(species)
print oaks_loops

oaks_lc = set([species for species in taxa if is_an_oak(species)]) #list comprehension which places oaks into set
print oaks_lc

#Names in upper case - for loop and list comprehension creating a set of just oak species but switching the text to upper case
oaks_loops = set() 
for species in taxa:
	if is_an_oak(species):
		oaks_loops.add(species.upper())
print oaks_loops

oaks_lc= set([species.upper() for species in taxa if is_an_oak(species)])
print oaks_lc
