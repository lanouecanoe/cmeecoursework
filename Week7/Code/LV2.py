#!/usr/bin/python

__Author__ = 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__Date__ = 'Nov 2017'

"""Script with modified Lokta-Volterra equation which can take arguments for r,
a,z,e values or can run with default values then creates pdf of resulting graph
"""

#importing packages
import scipy as sc 
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting
import sys #importing sys for multiple arguments


#Function which defines LV equations and starting pop size and time
def dR_dt(pops, t=0):
    """ Returns the growth rate of predator and prey populations at any 
    given time step """
    
    R = pops[0]
    C = pops[1]
    dRdt = r*R*(1-(R/K)) - a*R*C 
    dydt = -z*C + e*a*R*C
    
    return sc.array([dRdt, dydt])
    
global r,a,z,e,K #setting variables as global so they can be applied to all functions
if len(sys.argv) == 5: 			#listing number of arguments in command line
	r = float(sys.argv[1])
	a = float(sys.argv[2])
	z = float(sys.argv[3])
	e = float(sys.argv[4])
else:                         #setting default values 
	r = 1.
	a = 0.1
	z = 1.5
	e = 0.75 
#~ print [r,a,z,e] #printing variable values being used 

K = 200 # Carrying capacity

# Now define time -- integrate from 0 to 15, using 1000 points:
t = sc.linspace(0, 15,  1000)

#setting initial conditions
x0 = 10
y0 = 5 
z0 = sc.array([x0, y0]) # initials conditions: 10 prey and 5 predators per unit area

#integration
pops, infodict = integrate.odeint(dR_dt, z0, t, full_output=True)

infodict['message']     # >>> 'Integration successful.'

prey, predators = pops.T #defining pops.T array
f1 = p.figure() #Open empty figure object
p.plot(t, prey, 'g-', label='Resource density') # Plot
p.plot(t, predators  , 'b-', label='Consumer density')
p.text(1,26,'r=%r,a=%r,z=%r,e=%r,K=%r' %(r,a,z,e,K)) #adding text inside the plot
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-Resource population dynamics')
p.show()
f1.savefig('../Results/prey_and_predators_2.pdf') #Save figure

