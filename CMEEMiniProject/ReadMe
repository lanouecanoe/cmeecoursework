Mini Project 

Overview:
The aim of the mini project is to create a series of scripts which can input thermal performance data from a large data file and using that data, perform a non-linear least squares analysis and model selection. The scripts included sort through the data, removing any missing or poorly measured data, performing the NLLS, calculate AIC values for each model, plotting the original data with the projected curves, and compiling the report in LaTex. 

Code: 
fitting.py
    Python verion 2.7.12
    Packages: lmfit, numpy, numpy.polynomial.polynomial, pandas, subprocess
        - lmfit used for NLLS fitting and minimization
        - numpy used for creating arrays and mathematical commands
        - numpy.polynomial.polynomial used for NLLS of cubic model
        - pandas used to create and work with dataframes
        - subprocess used to run modification script from the fitting script
    Description: Python script which runs the modify_data.R script used to subset and modified TPC data and performs an NLLS analysis. The script outputs a new csv which contains the     minimized parameters, statistial values from the NLLS, along with the inital and        measured values
images.R
    R version 3.2.3
    Packages: ggplot2, reshape2, Dplyr
        - ggplot2 used to make graphs for results selection
        - reshape2 used to restructure dataframe to make graphs
        - dplyr used to calculate summary statistics
    Description: Script creating various figures for the results section
modify_data.R
    R version 3.2.3
    Packages: Dplyr
        - used to group data by ID and effectively sort through and calculate initial values without using for loops
    Description: R script which sorts through the raw data and calculates estimates for initial values for the NLLS  
plot_building.R
    R version 3.2.3 
    Packages: Dplyr, ggplot2
        - dplyr used to run plotting function in a more efficient way than using for loops
        - ggplot2 used to plot all the graphs
    Description: Script which inputs a csv file containing the minimized starting values from the NLLS and plots the expected curves with the measured data. Plots are saved in a Results subdirectory
run_MiniProject.py 
    Python verion 2.7.12
    Packages: subprocess, time
        - subprocess used to run all the R, bash and python scripts used in the mini project
        - time used to measure how long sections of the mini project take to run 
    Description: Runs all the scripts involved in the mini project and times how long the project takes to run
write_up.tex
    Description: Report write up file
write_up_compile.sh
    Packages: None
    Description: Bash script which runs the LaTex file containing the report, saves the resulting pdf in the Results directory and removes any auxiliary files created from the compilation

Data:
GrowthRespPhotoData_new.csv
modified_data.csv
data4plotting.csv

Results:
FitGraphs
    Description: subdirectory containing all thermal performance curves
Images
    Description: Figures created for the final report
write_up.pdf

Sandbox
    Description: Folder containing extra stuff and things that just didn't make the final cut
