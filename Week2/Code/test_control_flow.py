#!/usr/bin/env python

"""Some functions exemplifying the use of control statements
Excersise showing the use of doc strings and doc testing"""

__author__= 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__date__= '10/10/17'

import sys  
import doctest #showing that you're about to perform a doctest for bugs

def even_or_odd(x=0): #function determining if a number is odd or even

	"""Find whether a number x is even or odd. #doctests to ensure functions are running properly
	>>> even_or_odd(10)
	'10 is even!'
	
	>>> even_or_odd(5)
	'5 is odd!'
	
	whenever a float is provided, then the closest integer is used:
	>>> even_or_odd(3.2)
	'3 is odd!'
	
	in case of negative numbers, the positive is taken:
	>>> even_or_odd(-2)
	'-2 is even!'
	
	"""
	if x % 2 == 0:
		return "%d is even!" % x
	return "%d is odd!" % x 
	
def largest_divisor_five(x=120): #function determining which number (2,3,4,5) is the largest divisor of x 
	"""Find which is the largest divisor of x among 2,3,4,5."""
	largest = 0
	if x % 5 == 0:
		largest = 5 
	elif x % 4 == 0:
		largest = 4
	elif x % 3 == 0:
		largest = 3
	elif x % 2 == 0:
		largest = 2
	else: 
		return "No divisor found for %d" % x
	return "The largest divisor of %d is %d" % (x, largest)

def is_prime(x=70): #determining if a number is prime
	"""Find whether an integer is prime."""
	for i in range (2,x):
		if x % i == 0:
			print "%d is not a prime: %d is a divisor" % (x,i)
			
			return False
		print "%d is a prime!" % x
		return True
		
def find_all_primes(x=22): #finding all the prime numbers until 22
	"""Find all the primes up to x"""
	allprimes = []
	for i in range (2, x + 1):
		if is_prime(i):
			allprimes.append(i)
	print "There are %d primes between 2 and %d" % (len(allprimes), x)
	return allprimes
			
def main(argv): #defining the main arguments and the default values
	print even_or_odd (22)
	print even_or_odd (33)
	print largest_divisor_five (120)
	print largest_divisor_five (121)
	print is_prime (60)
	print is_prime (59)
	print find_all_primes (100)
	return 0
	
if (__name__ == "__main__"):
	status = main(sys.argv)
	sys.exit (status)
	
doctest.testmod() 
