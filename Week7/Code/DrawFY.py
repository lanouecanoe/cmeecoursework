#!/usr/bin/python

__Author__ = 'Julia Lanoue (j.lanoue17@imperial.ac.uk)'
__Date__ = 'Nov 2017'

"""Script which uses networkx to create a network graph
"""

#importing packages
import networkx as nx
import scipy as sc
import matplotlib.pyplot as plt

#defining function to generate random adjacency list
def GenRdmAdjList(N = 2, C = 0.5):
	"""
	Generate random adjacency list given N nodes with connectance probability C
	"""
	Ids = range(N) #defining range
	ALst = []  #creating empty list
	for i in Ids:
		if sc.random.uniform(0,1,1) < C:
			Lnk = sc.random.choice(Ids,2).tolist()
			if Lnk[0] != Lnk[1]:
				ALst.append(Lnk) #adding random values from the uniform distribution 
	return ALst
	
#Assign body mass range
SizRan = ([-10,10])

#defining connectance and maximum N value
MaxN = 30
C = 0.75

#Generate adjacency list:
AdjL = sc.array(GenRdmAdjList(MaxN, C))

#Generate species (node) data:
Sps = sc.unique(AdjL)
Sizs = sc.random.uniform(SizRan[0], SizRan[1], MaxN)

##### Plotting ######
plt.close('all')

#Calculate coordinates for circular configuration:
pos = nx.circular_layout(Sps)

G = nx.Graph()
G.add_nodes_from(Sps) #adding nodes
G.add_edges_from(tuple(AdjL)) #adding edges
NodSizes = 10**-32 + (Sizs-min(Sizs))/(max(Sizs)-min(Sizs)) #node sizes in proportion to the body sizes
nx.draw(G,pos,node_size = NodSizes*1000)
plt.show()
